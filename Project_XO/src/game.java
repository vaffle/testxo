import java.util.Scanner;

public class game {
	private player X;
	private player O;
	private board board;

	public game() {
		X = new player('X');
		O = new player('O');

	}

	public void play() {
		while (true) {
			playone();
		}
	}

	public void playone() {
		board = new board(X, O);
		showWellcome();

		while (true) {
			showTable();
			showTurn();
			input();
			if (board.isFinish()) {
				break;
			}
			board.switchPlayer();
		}
		showTable();
		showWinner();
		showStat();
	}

	private void showStat() {
		System.out.println(X.getName() + " Win,Draw,Lose : " + X.getWin() + "," + X.getDraw() + "," + X.getLose());
		System.out.println(O.getName() + " Win,Draw,Lose : " + O.getWin() + "," + O.getDraw() + "," + O.getLose());
		System.out.println();
	}

	private void showWinner() {
		player player = board.getWinner();
		System.out.println(player.getName() + " Win.");
	}

	private void showWellcome() {
		System.out.println("Wellcome to XO game.");
	}

	private void showTable() {
		char[][] table = board.getTable();
		System.out.println("  1 2 3");
		for (int i = 0; i < table.length; i++) {
			System.out.print(i + 1);
			for (int j = 0; j < table.length; j++) {
				System.out.print(" " + table[i][j]);
			}
			System.out.println();
		}
	}

	private void showTurn() {
		player player = board.getCurrentPlayer();
		System.out.println(player.getName() + " Turn..");
	}

	private void input() {
		Scanner kb = new Scanner(System.in);

		while (true) {
			try {
				System.out.print("Please input Row Col : ");
				String input = kb.nextLine();
				String[] str = input.split(" ");
				if (str.length != 2) {
					System.out.println("Please input Row Col [1-3] (ex : 1 2) ");
					continue;

				}
				int row = Integer.parseInt(str[0]) - 1;
				int col = Integer.parseInt(str[1]) - 1;

				if (board.setTable(row, col) == false) {
					System.out.println("Table is not empty!! ");
					continue;
				}
				break;
			} catch (Exception e) {
				System.out.println("Please input Row Col [1-3] (ex : 1 2) ");
				continue;

			}

		}

	}
}
